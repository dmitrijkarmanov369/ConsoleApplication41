﻿#include <iostream>
#include <stdlib.h>
using namespace std;
void main()
{
    setlocale(LC_ALL, "Rus");
    system("color 3F");

    const char Month[][12] = { "Январь","Февраль","Март","Апрель","Май  ","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" };

    int profit[12];
    int begin = 0;
    int end = 0;
    int max = begin;
    int min = begin;


    for (int i = 0; i < 12; i++)
    {
        cout << "Прибыль в : " << Month[i] << "\t";
        cin >> profit[i];
    }

    cout << "\n";
    cout << "Введите начальный диапазон месяцев от 1 до 12  : ";
    cin >> begin;
    cout << "Введите конечный диапазон месяцев от 1 до 12  : ";
    cin >> end;

    for (int i = begin; i <= end; i++)
    {
        cout << " " << profit[i];

        if (profit[max] <= profit[i])
            max = i;

        if (profit[min] >= profit[i])
            min = i;
    }
    cout << "\nМаксимальная прибыль в выбраном диапазоне будет в  " << Month[max] << "\t" << profit[max] << "\n\n";
    cout << "\nМинимальная прибыль в выбраном диапазоне будет в  " << Month[min] << "\t" << profit[min] << "\n\n";
}